# arc2mariadb

  

Earthworm module that reads TYPE_HYP2000 messages and stores it on a MariaDB database. It will also listen for TYPE_MAGNITUDE messages coming from *localmag* module and update magnitude values on table.
Database contains two tables: events and phases. Phases is related to events on it's event_id field. If an event is deleted from database, all it's related phases will be deleted too.

  


## Installation

- Clone Earthworm from source: 

        git clone https://gitlab.com/seismic-software/earthworm.git earthworm_git
    
- If you don't have a MariaDB server already running, you will need to install one. A good tutorial can be found here:

        https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-20-04
    
- You will also need to create a database, add an user and define a password. Run these commands in your MariaDB command line:

        create database myDBname;
        grant all privileges on myDBname.* TO 'myDBuser'@'localhost' identified by 'myDBpassword';
        grant all privileges on myDBname.* TO 'myDBuser'@'%' identified by 'myDBpassword';
        flush privileges;
        
- Install libmariadb-dev package:

        sudo apt install libmariadb-dev

- Export variables:

        export EW_INSTALL_VERSION=earthworm_git

- Source Earthworm environment variables for compiling:

        source earthworm_git/environment/ew_linux.bash

- cd to src/archiving/ folder.

        cd earthworm_git/src/archiving/


- ***OPTIONAL*** Clone arc2mariadb source files:
        
(Skip this step if you already have an *arc2mariadb* folder. I am submitting source code to Earthworm master branch)

        git clone https://gitlab.com/oavv/arc2mariadb.git

- Compile:
        
Sorry no Windows version yet. Feel free to contribute if you know how to link MariaDB C connector to source code.

        cd arc2mariadb
        make -f makefile.unix



- Watch for errors during compilation

  

## Usage

- Add *arc2mariadb* to *startstop* file. Don't forget to define a new module ID for **MOD_ARC2MARIADB** in *earthworm.d* file.
- Add *arc2mariadb.desc* to *statmgr.d*.
- Configure arc2mariadb.d file according to your needs.
- Watch for logs to see if module is properly connecting to MariaDB and writing events and phases to it:

        arc2mariadb: startup at UTC_20231221_14:26:44
        This program is using the MT-Safe version of logit.
        -------------------------------------------------------
        20231221_UTC_14:26:44 arc2mariadb(MOD_ARC2MARIADB): Read command file <archiving/arc2mariadb.d>
        20231221_UTC_14:26:44 arc2mariadb: connected to: localhost. Using database vickDB
        20231221_UTC_14:26:44 arc2mariadb: checking events table status...
        20231221_UTC_14:26:44 arc2mariadb: events table OK!
        20231221_UTC_14:26:44 arc2mariadb: checking phases table status...
        20231221_UTC_14:26:44 arc2mariadb: phases table OK!
        parse_arc: parsed HYPO summary message.
        parse_arc: parsed HYPO phase line (NIE.HHZ).
        parse_arc: parsed HYPO phase line (NIE.HHE).
        parse_arc: parsed HYPO phase line (BAR.HHZ).
        parse_arc: parsed HYPO phase line (BAR.HHE).
        parse_arc: parsed HYPO phase line (FEA.HHZ).
        parse_arc: parsed HYPO phase line (FEA.HHE).
        parse_arc: parsed HYPO phase line (CLD.HHZ).
        parse_arc: parsed HYPO phase line (RAY.HHN).
        parse_arc: parsed HYPO phase line (RAY.HHN).
        parse_arc: parsed HYPO phase line (CUR.HHN).
        parse_arc: parsed HYPO phase line (CUR.HHN).
        parse_arc: parsing HYPO phase lines is terminated.
        parse_arc: read 26 lines and 11 phases of the arc message.
        20231221_UTC_07:30:34 arc2mariadb debug: wrote event 1485 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 1/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 2/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 3/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 4/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 5/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 6/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 7/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 8/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 9/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 10/11 to database
        20231221_UTC_07:30:34 arc2mariadb debug: wrote phase 11/11 to database

- Use any database tool like **DBeaver** (https://dbeaver.io/download/) to see how database gets filled.

- Latitude and longitude are stored as a single field **SQL Geometry Type** (https://mariadb.com/kb/en/geometry-types/). So when using DBeaver, a map with the event location will be shown!!!.


## Support

I actively check Earthworm google groups:
https://groups.google.com/g/earthworm_forum


## Contributing

Feel free to contribute or suggest any modifications.

  
## Authors and acknowledgment

Coded by Victor Preatoni for OAVV-SEGEMAR (Observatorio Argentino de Vigilancia Volcanica | Servicio Geologico Minero Argentino).
Thanks to Gabriela Badi (UNLP) for helping me interpret Fred Klein's HYPOINVERSE-2000 manual. Thanks to Jay Wellik (USGS) for helping with the setup of an Earthworm test system and tune-up of parameters.
