/*
 *   $Id: arc2mariadb.c
 *
 *    Revision history:
 *
 *     Revision 1.0  2023/11/28 victor preatoni
 *     Initial revision
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <signal.h>
#include <mariadb/mysql.h>

#include <time_ew.h>
#include <chron3.h>
#include <earthworm.h>
#include <kom.h>
#include <transport.h>
#include <mem_circ_queue.h>
#include <read_arc.h>
#include <rw_mag.h>

#include "arc2mariadb.h"

/* Thread things
***************/
static ew_thread_t tidDup;           /* Dup thread id */
static ew_thread_t tidStacker;       /* Thread moving messages from transport to queue */
static volatile int MessageStackerStatus = MSGSTK_OFF;
static QUEUE OutQueue;               /* from queue.h, queue.c; sets up linked    */
                                     /*    list via malloc and free              */
                                     
/* Message Buffers to be allocated
*********************************/
static char *Rdmsg = NULL;           /* msg retrieved from transport      */
static char *Wrmsg = NULL;           /*  message to write to file   */
static SHM_INFO  InRegion;           /* shared memory region to use for input    */

/* Timers
******/
static time_t MyLastBeat;            /* time of last local (into Earthworm) hearbeat */


/************* GLOBAL VARIABLES ***********/
static char *ExecName;        /* Executable name */
static pid_t MyPid;		          /* Our own pid, sent with heartbeat for restart purposes */

static Config   config = {0};
static LookUp   lookup = {0};
static MYSQL *mariadb_conn;  // Global variable for the database connection

// Global variable holding the CREATE TABLE statement for events
const char eventsCreateStatement[] = {
    "(event_id BIGINT UNSIGNED NOT NULL, "
    "method VARCHAR(20) DEFAULT NULL, "
    "date DATETIME(6) NOT NULL, "
    "position POINT NOT NULL, "
    "depth FLOAT DEFAULT NULL, "
    "magnitude FLOAT DEFAULT NULL, "
    "magType VARCHAR(5) DEFAULT NULL, "
    "phases INT DEFAULT NULL, "
    "stdError FLOAT DEFAULT NULL, "
    "azimuthalGap INT DEFAULT NULL, "
    "minimumDistance FLOAT DEFAULT NULL, "
    "e0 FLOAT DEFAULT NULL, "
    "e0az INT DEFAULT NULL, "
    "e0dip INT DEFAULT NULL, "
    "e1 FLOAT DEFAULT NULL, "
    "e1az INT DEFAULT NULL, "
    "e1dip INT DEFAULT NULL, "
    "e2 FLOAT DEFAULT NULL, "
    "erh FLOAT DEFAULT NULL, "
    "erz FLOAT DEFAULT NULL, "
    "PRIMARY KEY (event_id), "
    "SPATIAL INDEX position_index (position), "
    "UNIQUE KEY events_UN (date, position, method)"
    ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci"
};

// Global variable holding the CREATE TABLE statement for phases
const char phasesCreateStatement[] = {
    "(id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, "
    "date DATETIME(6) NOT NULL, "
    "uncertainty FLOAT DEFAULT NULL, "
    "station VARCHAR(5) NOT NULL, "
    "channel VARCHAR(4) NOT NULL, "
    "network VARCHAR(3) NOT NULL, "
    "location VARCHAR(3) NOT NULL, "
    "onset VARCHAR(20) DEFAULT NULL, "
    "polarity VARCHAR(20) DEFAULT NULL, "
    "phase VARCHAR(5) DEFAULT NULL, "
    "event_id BIGINT UNSIGNED DEFAULT NULL, "
    "azimuth SMALLINT UNSIGNED DEFAULT NULL, "
    "distance FLOAT DEFAULT NULL, "
    "takeoffangle SMALLINT UNSIGNED DEFAULT NULL, "
    "timeresidual DOUBLE DEFAULT NULL, "
    "timeweight DOUBLE DEFAULT NULL, "
    "evaluation VARCHAR(20) DEFAULT NULL, "
    "PRIMARY KEY (id), "
    "UNIQUE KEY phases_UN (station, channel, network, location, phase, event_id)"
    ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci"
};



static int sec1600toStr(double secs1600, char * str)
{
    double integer;
    time_t ot;
	struct tm *timeinfo;
    unsigned short milliseconds;
    size_t size;
    
    ot = (time_t)(secs1600 - GSEC1970);
    milliseconds = modf(secs1600, &integer) * (unsigned int)1000;
    timeinfo = gmtime(&ot);
    size = strftime(str, MAX_STR, "%Y-%m-%d %H:%M:%S", timeinfo);
    size += snprintf(str + size, MAX_STR - size, ".%03d", milliseconds);
    
    return size;
}



int main(int argc, char **argv)
{
    /* Other variables: */
    long     recsize;	  /* size of retrieved message                */
    MSG_LOGO reclogo;	  /* logo of retrieved message                */
    time_t   now;	      /* current time, used for timing heartbeats */
    char     errText[MAX_STR];  /* string for log/error/heartbeat messages  */

    /* Check command line arguments
     ******************************/
    ExecName = argv[0];
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <configfile>\n", ExecName);
        fprintf(stderr, "Version: %s\n", ARC2MARIADB_VERSION);
        exit(-1);
    }

    /* Initialize name of log-file & open it
     ****************************************/
    logit_init(argv[1], 0, 512, 1);

    /* Read the configuration file(s)
     ********************************/
    if (arc2mariadb_config(argv[1], &config)) {
        logit("e" ,"%s: exiting!\n", ExecName);
        exit(-1);
    }
    
    logit("et", "%s(%s): Read command file <%s>\n", ExecName, config.MyModName, argv[1]);

    /* Look up important info from earthworm.h tables
     *************************************************/
    if (arc2mariadb_lookup(&config, &lookup)) {
        logit("e" ,"%s: exiting!\n", ExecName);
        exit(-1);
    }
    
    
    /* Connect to MariaDB database
     ******************************/
    if (arc2mariadb_startDB(&config)) {
        logit("e", "%s: Cannot open database: %s; exiting!\n", ExecName, mysql_error(mariadb_conn));
        arc2mariadb_closeDB();
        exit(-1);
    }
    else
        logit("et", "%s: connected to: %s. Using database %s\n", ExecName, config.DBHostname, config.DBName);
    
    
    
    /* Create tables in database if they don't exist
     ***********************************************/
    logit("et", "%s: checking %s table status...\n", ExecName, config.DBeventsTable);
    if (arc2mariadb_checkTable(config.DBeventsTable)) {
        /* Create table for events */
        logit("et", "%s: table %s does not exist. Creating...\n", ExecName, config.DBeventsTable);
        if (arc2mariadb_createTable(config.DBeventsTable, NULL, eventsCreateStatement)) {
            logit("e", "%s: Cannot create table. Error code: %s; exiting!\n", ExecName, mysql_error(mariadb_conn));
            exit(-1);
        }
    }
    logit("et", "%s: %s table OK!\n", ExecName, config.DBeventsTable);
    
    logit("et", "%s: checking %s table status...\n", ExecName, config.DBphasesTable);
    if (arc2mariadb_checkTable(config.DBphasesTable)) {
        /* Create table for phase arrivals */
        logit("et", "%s: table %s does not exist. Creating...\n", ExecName, config.DBphasesTable);
        if (arc2mariadb_createTable(config.DBphasesTable, config.DBeventsTable, phasesCreateStatement)) {
            logit("e", "%s: Cannot create table. Error code: %s; exiting!\n", ExecName, mysql_error(mariadb_conn));
            exit(-1);
        }
    }
    logit("et", "%s: %s table OK!\n", ExecName, config.DBphasesTable);
    
    

    /* Reinitialize the logging level
     *********************************/
    logit_init(argv[1], 0, 512, config.LogSwitch);

    /* Get our own Pid for restart purposes
     ***************************************/
    MyPid = getpid();
    if(MyPid == -1) {
        logit("e", "%s(%s): Cannot get pid; exiting!\n", ExecName, config.MyModName);
        exit(-1);
    }

    /* Allocate space for input/output messages for all threads
     ***********************************************************/
    /* Buffer for Read thread: */
    if ((Rdmsg = (char *)malloc(config.MaxMsgSize + 1)) ==  NULL) {
        logit("e", "%s(%s): error allocating Rdmsg; exiting!\n", ExecName, config.MyModName);
        exit(-1);
    }

    /* Buffers for Write thread: */
    if ((Wrmsg = (char *)malloc(config.MaxMsgSize + 1)) ==  NULL) {
        logit("e", "%s(%s): error allocating Wrmsg; exiting!\n", ExecName, config.MyModName);
        exit(-1);
    }

    /* Create a Mutex to control access to queue
     ********************************************/
    CreateMutex_ew();

    /* Initialize the message queue
     *******************************/
    initqueue(&OutQueue, config.QueueSize, config.MaxMsgSize + 1);

    /* Attach to Input/Output shared memory ring
     ********************************************/
    tport_attach(&InRegion, lookup.InRingKey);

    /* step over all messages from transport ring
     *********************************************/
    while (tport_getmsg(&InRegion, lookup.GetLogo, lookup.nLogo, &reclogo, &recsize, Rdmsg, config.MaxMsgSize) != GET_NONE);


    /* One heartbeat to announce ourselves to statmgr
     ************************************************/
    time(&MyLastBeat);
    sprintf(errText, "%ld %ld\n", (long)MyLastBeat, (long)MyPid);
    arc2mariadb_status(&lookup.HeartLogo, errText);

    /* Start the message stacking thread if it isn't already running.
     ****************************************************************/
    if (MessageStackerStatus != MSGSTK_ALIVE) {
        if (StartThread(MessageStacker, (unsigned)THREAD_STACK, &tidStacker) == -1) {
            logit("e", "%s(%s): Error starting  MessageStacker thread; exiting!\n", ExecName, config.MyModName);
            tport_detach(&InRegion);
            exit(-1);
        }
        MessageStackerStatus = MSGSTK_ALIVE;
    }

    /* Start the writing thread
     ***********************************/
    if (StartThread(WriteThd, (unsigned)THREAD_STACK, &tidDup) == -1) {
        logit("e", "%s(%s): Error starting Write thread; exiting!\n", ExecName, config.MyModName);
        tport_detach(&InRegion);
        exit(-1);
    }

    /* Start main arc2mariadb service loop, which aimlessly beats its heart.
     **********************************/
    while (tport_getflag(&InRegion) != TERMINATE  && tport_getflag(&InRegion) != MyPid) {
        /* Beat the heart into the transport ring
            ****************************************/
        time(&now);
        if (difftime(now, MyLastBeat) > (double)config.HeartBeatInt) {
            sprintf(errText, "%ld %ld\n", (long)now, (long)MyPid);
            arc2mariadb_status(&lookup.HeartLogo, errText);
            MyLastBeat = now;
            /* ping the MariaDB connection to avoid closure */
	    mysql_ping(mariadb_conn);
        }

        /* take a brief nap; added 970624:ldd
            ************************************/
        sleep_ew(500);
    } /*end while of monitoring loop */

    /* Shut it down
     ***************/
    tport_detach( &InRegion );
    logit("t", "%s(%s): termination requested; exiting!\n", ExecName, config.MyModName);
    arc2mariadb_closeDB();
    exit(0);
}
/* *******************  end of main *******************************
******************************************************************/



/**************************  Main Write Thread   ***********************
 *          Pull a messsage from the queue, and write it to database  *
 **********************************************************************/
thr_ret WriteThd(void *dummy)
{
	MSG_LOGO reclogo;
	int ret;
	int i;
	long msgSize;
    SQLeventEntry event; /* Where to hold SQL event entry */
    SQLphaseEntry * phases; /* Pointer to phases array; event.phases will dictate array lenght */
    SQLphaseEntry * ptr;
    
    
    while (1) {   /* main loop */
        /* Get message from queue
         *************************/
        RequestMutex();
        ret = dequeue(&OutQueue, Wrmsg, &msgSize, &reclogo);
        ReleaseMutex_ew();

        if (ret < 0) { /* -1 means empty queue */
            sleep_ew(500);
            continue;
        }

        /* Determine which GetLogo this message used */
        for (i = 0; i < lookup.nLogo; i++) {
            if ((lookup.GetLogo[i].type == WILD || lookup.GetLogo[i].type == reclogo.type) &&
                (lookup.GetLogo[i].mod == WILD || lookup.GetLogo[i].mod == reclogo.mod) &&
                (lookup.GetLogo[i].instid == WILD || lookup.GetLogo[i].instid == reclogo.instid)) {
                break; /* Found valid logo, quit for loop */
            }
        }
        
        if (i == lookup.nLogo) {
            logit("et", "%s error: logo <%d.%d.%d> not found\n", ExecName, reclogo.instid, reclogo.mod, reclogo.type);
            continue;
        }
        
        
        if (reclogo.type == lookup.TypeHyp2000Arc) { /* Got a TYPE_HYP2000ARC message */
            ret = arc2mariadb_parseHyp2000(Wrmsg, &event, &phases); 
            if (ret < 0) {
                logit("et", "%s error: cannot parse ARC message\n", ExecName);
                continue;
            }
            
            /* *phases ptr is allocated by arc2mariadb_parseHyp2000.
             * Remember to free() it if continuing while due to an error */
            
            if (arc2mariadb_writeEvent(&event, config.DBeventsTable)) {
                logit("et", "%s error: cannot write event to database %s\n", ExecName, mysql_error(mariadb_conn));
                goto free;
            }
            else if (config.DebugSwitch)
                logit("et", "%s debug: wrote event %lu to database\n", ExecName, event.event_id);
            
            /* Iterate thru phases */
            for (ptr = phases, i = 0; i < ret; i++, ptr++) {
                if (arc2mariadb_writePhase(ptr, config.DBphasesTable)) {
                    logit("et", "%s error: cannot write phase to database %s\n", ExecName, mysql_error(mariadb_conn));
                    continue;
                }
                else if (config.DebugSwitch)
                    logit("et", "%s debug: wrote phase %d/%d to database\n", ExecName, i+1, ret);
            }
free:       free(phases);
        }
        
        else if (reclogo.type == lookup.TypeMagnitude) { /* Got a TYPE_MAGNITUDE message */
            if (arc2mariadb_parseMagitude(Wrmsg, config.DBeventsTable)) {
                logit("et", "%s error: cannot write magnitude to database %s\n", ExecName, mysql_error(mariadb_conn));
                continue;
            }
            else if (config.DebugSwitch)
                logit("et", "%s debug: wrote magnitude on event\n", ExecName);
        }
        
    }   /* End of main loop */
}



/********************** Message Stacking Thread *******************
 *           Move messages from transport to memory queue         *
 ******************************************************************/
thr_ret MessageStacker(void *dummy )
{
    long         recsize;	/* size of retrieved message             */
    MSG_LOGO     reclogo;       /* logo of retrieved message             */
    time_t       now;
    char         errText[MAX_STR]; /* string for log/error/heartbeat messages */
    int          ret;
    int          NumOfTimesQueueLapped= 0; /* number of messages lost due to queue lap */

    /* Tell the main thread we're ok
     ********************************/
    MessageStackerStatus = MSGSTK_ALIVE;

    /* Start main service loop for current connection
     ************************************************/
    while (1) {
        /* Get a message from transport ring
            ************************************/
        ret = tport_getmsg(&InRegion, lookup.GetLogo, lookup.nLogo, &reclogo, &recsize, Rdmsg, config.MaxMsgSize);

        switch (ret) {
        case GET_NONE:
            /* Wait if no messages for us */
            sleep_ew(100);
            continue;
            break;
        case GET_TOOBIG:
            time(&now);
            sprintf(errText, "%ld %hd msg[%ld] i%d m%d t%d too long for target", (long)now, (short)ERR_TOOBIG, recsize, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type);
            arc2mariadb_status(&lookup.ErrorLogo, errText);
            continue;
            break;
        case GET_MISS:
            time(&now);
            sprintf(errText, "%ld %hd missed msg(s) i%d m%d t%d in %s", (long)now, (short)ERR_MISSMSG, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type, config.InRing);
            arc2mariadb_status(&lookup.ErrorLogo, errText);
            break;
        case GET_NOTRACK:
            time(&now);
            sprintf( errText, "%ld %hd no tracking for logo i%d m%d t%d in %s", (long)now, (short)ERR_NOTRACK, (int) reclogo.instid, (int) reclogo.mod, (int)reclogo.type, config.InRing);
            arc2mariadb_status(&lookup.ErrorLogo, errText);
            break;
        }

        /* Process retrieved msg (ret==GET_OK,GET_MISS,GET_NOTRACK)
        ***********************************************************/
        Rdmsg[recsize] = '\0';

        /* put it into the 'to be shipped' queue */
        /* the Write thread is in the biz of de-queueng and writing */
        RequestMutex();
        ret = enqueue(&OutQueue, Rdmsg, recsize, reclogo);
        ReleaseMutex_ew();

        switch(ret) {
        case -2:
            /* Serious: quit */
            /* Currently, eneueue() in mem_circ_queue.c never returns this error. */
            time(&now);
            sprintf(errText, "%ld %hd internal queue error. Terminating.", (long)now, (short)ERR_QUEUE);
            arc2mariadb_status(&lookup.ErrorLogo, errText);
            goto error;
            break;
        case -1:
            time(&now);
            sprintf(errText,"%ld %hd message too big for queue.", (long)now, (short)ERR_QUEUE);
            arc2mariadb_status(&lookup.ErrorLogo, errText);
            continue;
            break;
        case -3:
            NumOfTimesQueueLapped++;
            if (!(NumOfTimesQueueLapped % 5)) {
                logit("t", "%s(%s): Circular queue lapped 5 times. Messages lost.\n", ExecName, config.MyModName);
                if (!(NumOfTimesQueueLapped % 100)) {
                    logit("et", "%s(%s): Circular queue lapped 100 times. Messages lost.\n", ExecName, config.MyModName);
                }
            }
            continue;
        }
    } /* end of while */

    /* we're quitting
     *****************/
error:
    MessageStackerStatus = MSGSTK_ERR; /* file a complaint to the main thread */
    KillSelfThread(); /* main thread will not restart us */
    return THR_NULL_RET; /* Should never get here */
}




/*****************************************************************************
 *  arc2mariadb_config() processes command file(s) using kom.c functions;        *
 *                    -1 if any errors are encountered.	             *
 *****************************************************************************/
int arc2mariadb_config(char * configfile, Config * configPtr)
{
    char    *com;
    int      nfiles;
    int      success;
    char    *str;
    unsigned char errors;
    char errBuffer[MAX_STR * 4];

    /* Open the main configuration file
     **********************************/
    nfiles = k_open(configfile);
    if (nfiles == 0) {
        logit("e" ,"%s: Error opening command file <%s>\n", ExecName, configfile);
        return -1;
    }
    
    /* Load default values */
    configPtr->MaxMsgSize = MAX_BYTES_PER_EQ;
    configPtr->DBPort = MARIADB_DEFAULT_PORT;

    /* Process all command files
    ***************************/
    while (nfiles > 0) {   /* While there are command files open */
        while (k_rd()) {       /* Read next line from active file  */
            com = k_str();         /* Get the first token from line */

            /* Ignore blank lines & comments
            *******************************/
            if (!com)
                continue;
            if (com[0] == '#')
                continue;

            /* Open a nested configuration file
            **********************************/
            if(com[0] == '@') {
                success = nfiles + 1;
                nfiles = k_open(&com[1]);
                if (nfiles != success) {
                    logit("e" , "%s: Error opening command file <%s>\n", ExecName, &com[1]);
                    return -1;
                }
                continue;
            }

            /* Process anything else as a command
            ************************************/
            if (k_its("LogFile")) {
                configPtr->LogSwitch = k_int();
            }
            
            else if (k_its("Debug")) {
                configPtr->DebugSwitch = k_int();
            }
            
            else if (k_its("MyModuleId")) {
                if ((str = k_str()) != NULL) 
                    configPtr->MyModName = strdup(str);
            }
            
            else if (k_its("InRing")) {
                if ((str = k_str()) != NULL) 
                    configPtr->InRing = strdup(str);
            }
            
            else if (k_its("HeartBeatInt")) {
                configPtr->HeartBeatInt = k_int();
            }

            /* Enter installation & module & message types to get
            ****************************************************/
            else if (k_its("GetMsgLogo")) {
                if (configPtr->nLogo >= MAX_LOGO) {
                    logit("e", "%s: Too many <GetMsgLogo> commands in <%s>; max=%d; ignoring...\n", ExecName, configfile, MAX_LOGO);
                    continue;
                }
                if ((str = k_str()) != NULL)
                    configPtr->MsgLogo[configPtr->nLogo].InstID = strdup(str);
                if ((str = k_str()) != NULL)
                    configPtr->MsgLogo[configPtr->nLogo].ModID = strdup(str);
                if ((str = k_str()) != NULL)
                    configPtr->MsgLogo[configPtr->nLogo].MsgType = strdup(str);
                configPtr->nLogo++;
            }

            /* Maximum number of messages in outgoing circular buffer
            ********************************************************/
            else if (k_its("QueueSize")) {
                configPtr->QueueSize = k_long();
            }
            
            /* Custom module parameters
            ***************************/
            else if (k_its("DBUsername")) {
                if ((str = k_str()) != NULL) 
                    configPtr->DBUsername = strdup(str);
            }
            else if (k_its("DBHostname")) {
                if ((str = k_str()) != NULL) 
                    configPtr->DBHostname = strdup(str);
            }
            else if (k_its("DBName")) {
                if ((str = k_str()) != NULL) 
                    configPtr->DBName = strdup(str);
            }
            else if (k_its("DBPassword")) {
                if ((str = k_str()) != NULL) 
                    configPtr->DBPassword = strdup(str);
            }
            else if (k_its("DBeventsTable")) {
                if ((str = k_str()) != NULL) 
                    configPtr->DBeventsTable = strdup(str);
            }
            else if (k_its("DBphasesTable")) {
                if ((str = k_str()) != NULL) 
                    configPtr->DBphasesTable = strdup(str);
            }
            else if (k_its("MethodTag")) {
                if ((str = k_str()) != NULL) 
                    configPtr->MethodTag = strdup(str);
            }
            else if (k_its("EvaluationTag")) {
                if ((str = k_str()) != NULL) {
                    configPtr->EvaluationChar = str[0];
                    if ((str = k_str()) != NULL) {
                        configPtr->EvaluationTag = strdup(str);
                    }
                }
            }
            else if (k_its("WeightTable")) {
                configPtr->WeightTable[0] = (float)k_val();
                configPtr->WeightTable[1] = (float)k_val();
                configPtr->WeightTable[2] = (float)k_val();
                configPtr->WeightTable[3] = (float)k_val();
            }
            else if (k_its("DBPort")) {
                configPtr->DBPort = k_int();
            }
                    

            /* Unknown command
            *****************/
            else {
                logit("e", "%s: <%s> Unknown command in <%s>.\n", ExecName, com, configfile);
                continue;
            }

            /* See if there were any errors processing the command
            *****************************************************/
            if (k_err()) {
                logit("e", "%s: Bad <%s> command  in <%s>\n", ExecName, com, configfile);
                return -1;
            }
        }
        nfiles = k_close();
    }

    /* After all files are closed, check for missed commands
     *******************************************************/
    errors = 0;
    
    if (configPtr->nLogo == 0) {
        strcat(errBuffer, "<GetMsgLogo> ");
        errors++;
    }
    if (configPtr->MyModName == NULL) {
        strcat(errBuffer, "<MyModuleId> ");
        errors++;
    }
    if (configPtr->InRing == NULL) {
        strcat(errBuffer, "<InRing> ");
        errors++;
    }
    if (configPtr->HeartBeatInt == 0) {
        strcat(errBuffer, "<HeartBeatInt> ");
        errors++;
    }
    if (configPtr->QueueSize == 0) {
        strcat(errBuffer, "<QueueSize> ");
        errors++;
    }
    if (configPtr->DBHostname == NULL) {
        strcat(errBuffer, "<DBHostname> ");
        errors++;
    }
    if (configPtr->DBName == NULL) {
        strcat(errBuffer, "<DBName> ");
        errors++;
    }
    if (configPtr->DBUsername == NULL) {
        strcat(errBuffer, "<DBUsername> ");
        errors++;
    }
    if (configPtr->DBPassword == NULL) {
        strcat(errBuffer, "<DBPassword> ");
        errors++;
    }
    if (configPtr->DBeventsTable == NULL) {
        strcat(errBuffer, "<DBeventsTable> ");
        errors++;
    }
    if (configPtr->DBphasesTable == NULL) {
        strcat(errBuffer, "<DBphasesTable> ");
        errors++;
    }
    if (configPtr->MethodTag == NULL) {
        strcat(errBuffer, "<MethodTag> ");
        errors++;
    }
    if (configPtr->EvaluationTag == NULL || configPtr->EvaluationChar == 0) {
        strcat(errBuffer, "<EvaluationTag> ");
        errors++;
    }
    
    if (errors) {
        logit("e", "%s: ERROR, no %s command(s) in <%s>\n", ExecName, errBuffer, configfile);
        return -1;
    }

    return 0;
}


/****************************************************************************
 *  arc2mariadb_lookup( )   Look up important info from earthworm.h tables      *
 ****************************************************************************/
int arc2mariadb_lookup(Config * configPtr, LookUp * lookupPtr)
{
    /* Look up keys to shared memory regions
     *************************************/
    if ((lookupPtr->InRingKey = GetKey(configPtr->InRing)) == -1 ) {
        logit("e", "%s:  Invalid ring name <%s>\n", ExecName, configPtr->InRing);
        return -1;
    }

    /* Look up installations of interest
     *********************************/
    if (GetLocalInst(&lookupPtr->HeartLogo.instid) != 0) {
        logit("e", "%s: error getting local installation id\n", ExecName);
        return -1;
    }
    lookupPtr->ErrorLogo.instid = lookupPtr->HeartLogo.instid;

    /* Look up modules of interest
     ***************************/
    if (GetModId(configPtr->MyModName, &lookupPtr->HeartLogo.mod) != 0) {
        logit("e", "%s: Invalid module name <%s>\n", ExecName, configPtr->MyModName);
        return -1;
    }
    lookupPtr->ErrorLogo.mod = lookupPtr->HeartLogo.mod;

    /* Look up message types of interest
     *********************************/
    if (GetType("TYPE_HEARTBEAT", &lookupPtr->HeartLogo.type) != 0) {
        logit("e", "%s: Invalid message type <TYPE_HEARTBEAT>\n", ExecName);
        return -1;
    }
    if (GetType("TYPE_ERROR", &lookupPtr->ErrorLogo.type) != 0) {
        logit("e", "%s: Invalid message type <TYPE_ERROR>\n", ExecName);
        return -1;
    }
    lookupPtr->TypeError = lookupPtr->ErrorLogo.type;

    if (GetType("TYPE_HYP2000ARC", &lookupPtr->TypeHyp2000Arc) != 0) {
        logit("e", "%s: Invalid message type <TYPE_HYP2000ARC>\n", ExecName);
        return -1;
    }
    
    if (GetType("TYPE_MAGNITUDE", &lookupPtr->TypeMagnitude) != 0) {
        logit("e", "%s: Invalid message type <TYPE_MAGNITUDE>\n", ExecName);
        return -1;
    }

    /* Look up message logos
     ***********************/
    for (int n = 0; n < configPtr->nLogo; n++) {
        MSG_LOGO GetLogoTmp;
        
        if (GetInst(configPtr->MsgLogo[n].InstID, &GetLogoTmp.instid) != 0) {
            logit("e" , "%s: Invalid installation name <%s> in <GetMsgLogo> cmd\n", ExecName, configPtr->MsgLogo[n].InstID);
            return -1;
        }
        
        if (GetModId(configPtr->MsgLogo[n].ModID, &GetLogoTmp.mod) != 0) {
            logit("e" , "%s: Invalid module name <%s> in <GetMsgLogo> cmd\n", ExecName, configPtr->MsgLogo[n].ModID);
            return -1;
        }
        
        if (GetType(configPtr->MsgLogo[n].MsgType, &GetLogoTmp.type) != 0) {
            logit("e" , "%s: Invalid msgtype <%s> in <GetMsgLogo> cmd\n", ExecName, configPtr->MsgLogo[n].MsgType);
            return -1;
        }
        
        lookupPtr->GetLogo[n].instid = GetLogoTmp.instid;   
        lookupPtr->GetLogo[n].mod = GetLogoTmp.mod;
        lookupPtr->GetLogo[n].type = GetLogoTmp.type;
        lookupPtr->nLogo++;
    }
    
    return 0;
}

/***************************************************************************
 * arc2mariadb_status() sends an error or heartbeat message to transport    *
 *    If the message is of TYPE_ERROR, the text will also be logged.       *
 *    Since arc2mariadb_status is called be more than one thread, the logo  *
 *    and message must be constructed by the caller.                       *
 ***************************************************************************/
void arc2mariadb_status(MSG_LOGO *logo, char *msg)
{
    if (logo->type == lookup.TypeError)
        logit("et", "%s\n", msg);

    /* Write the message to shared memory
     ************************************/
    if (tport_putmsg(&InRegion, logo,  strlen(msg), msg) != PUT_OK) {
        logit("et", "%s:  Error sending message to transport.\n", ExecName);
    }
}




/***************************************************************************
 * arc2mariadb_startDB
 ***************************************************************************/
int arc2mariadb_startDB(Config * configPtr)
{
    my_bool reconnect;

    // Initialize the MySQL connection
    mariadb_conn = mysql_init(NULL);

    // Enable automatic reconnection
    reconnect = 1;
    if (mysql_options(mariadb_conn, MYSQL_OPT_RECONNECT, &reconnect) != 0) {
        return -1;
    }
    // Connect to the database
    if (mysql_real_connect(mariadb_conn, configPtr->DBHostname, configPtr->DBUsername, configPtr->DBPassword, configPtr->DBName, configPtr->DBPort, NULL, 0) == NULL) {
        return -1;
    }
    
    return 0;
}


/***************************************************************************
 * arc2mariadb_closeDB
 ***************************************************************************/
void arc2mariadb_closeDB(void)
{
    if (mariadb_conn != NULL) {
        mysql_close(mariadb_conn);
        mariadb_conn = NULL;
    }
}


/***************************************************************************
 * arc2mariadb_parseHyp2000
 * Returns: -1 on error
 *           0 if out of bounds
 *           N number of phases
 ***************************************************************************/
int arc2mariadb_parseHyp2000(char * msg, SQLeventEntry * event, SQLphaseEntry ** phases)
{
    HypoArc arc;		        /* a hypoinverse ARC message currently being processed */
    char timestr[MAX_STR];
    char label;
    char onset;
    double at;
    float res;
    int qual;
    char fm;
    float wt;
    char magType[3];
    int n;
    SQLphaseEntry *phasesPtr;
    
	
    if (parse_arc(msg, &arc))
        return -1;
    
    if (sec1600toStr(arc.sum.ot, timestr) < 20) // Wrong date
        return -1;
    
    *phases = malloc(arc.num_phases * sizeof(SQLphaseEntry));
    if (*phases == NULL) // No memory
        return -1;
    
    if (arc.sum.mdtype == 'D')
        strncpy(magType, "Md", sizeof(magType));
        
    
    event->event_id = arc.sum.qid;
    strncpy(event->method, config.MethodTag, sizeof(event->method));
    strncpy(event->date, timestr, sizeof(event->date));
    event->lat = arc.sum.lat;
    event->lon = arc.sum.lon;
    event->depth = arc.sum.z;
    event->magnitude = arc.sum.Md;
    strncpy(event->magType, magType, sizeof(event->magType));
//    event->phases = arc.sum.nph; /* Number of phases with qual > 0.1 */
    event->phases = arc.sum.nphtot; /* Total number of phases */
    event->stdError = arc.sum.rms;
    event->azimuthalGap = arc.sum.gap;
    event->minimumDistance = arc.sum.dmin;
    event->e0 = arc.sum.e0;
    event->e0az = arc.sum.e0az;
    event->e0dip = arc.sum.e0dp;
    event->e1 = arc.sum.e1;
    event->e1az = arc.sum.e1az;
    event->e1dip = arc.sum.e1dp;
    event->e2 = arc.sum.e2;
    event->erh = arc.sum.erh;
    event->erz = arc.sum.erz;
    
    
    /* Iterate thru phases */
    for (phasesPtr = *phases, n = 0; n < arc.num_phases; n++, phasesPtr++) {
        
        if (arc.phases[n]->Plabel == 'P') {
            label = arc.phases[n]->Plabel;
            at = arc.phases[n]->Pat;
            onset = arc.phases[n]->Ponset;
            res = arc.phases[n]->Pres;
            qual = arc.phases[n]->Pqual;
            fm = arc.phases[n]->Pfm;
            wt = arc.phases[n]->Pwt;
        }
        
        else if (arc.phases[n]->Slabel == 'S') {
            label = arc.phases[n]->Slabel;
            at = arc.phases[n]->Sat;
            onset = arc.phases[n]->Sonset;
            res = arc.phases[n]->Sres;
            qual = arc.phases[n]->Squal;
            fm = arc.phases[n]->Sfm;
            wt = arc.phases[n]->Swt;
        }
        else
            continue; //Wrong label
        
        if (sec1600toStr(at, timestr)  < 20)
            continue; //Skip wrong date
        
        if (qual >= 0 && qual <=3) {
            phasesPtr->uncertainty = config.WeightTable[qual];
        }
        
        switch (fm) {
            case 'U':
                strncpy(phasesPtr->polarity, "positive", sizeof(phasesPtr->polarity));
                break;
            case 'D':
                strncpy(phasesPtr->polarity, "negative", sizeof(phasesPtr->polarity));
                break;
            default:
                strncpy(phasesPtr->polarity, "?", sizeof(phasesPtr->polarity));
        }
        
        switch (onset) {
            case 'I':
                strncpy(phasesPtr->onset, "impulsive", sizeof(phasesPtr->onset));
                break;
            case 'E':
                strncpy(phasesPtr->onset, "emergent", sizeof(phasesPtr->onset));
                break;
            default:
                strncpy(phasesPtr->onset, "?", sizeof(phasesPtr->onset));
        }
        
        phasesPtr->event_id = event->event_id;
        strncpy(phasesPtr->date, timestr, sizeof(phasesPtr->date));
        strncpy(phasesPtr->s, arc.phases[n]->site, sizeof(phasesPtr->s));   
        strncpy(phasesPtr->c, arc.phases[n]->comp, sizeof(phasesPtr->c));
        strncpy(phasesPtr->n, arc.phases[n]->net, sizeof(phasesPtr->n));
        strncpy(phasesPtr->l, arc.phases[n]->loc, sizeof(phasesPtr->l));
        phasesPtr->phase = label;
        phasesPtr->azimuth = arc.phases[n]->azm;
        phasesPtr->distance = arc.phases[n]->dist;
        phasesPtr->takeoffangle = arc.phases[n]->takeoff;
        phasesPtr->timeresidual = res;
        phasesPtr->timeweight = wt;
        
        if (arc.phases[n]->datasrc == config.EvaluationChar)
            strncpy(phasesPtr->evaluation, config.EvaluationTag, sizeof(phasesPtr->evaluation));
        else
            strncpy(phasesPtr->evaluation, "?", sizeof(phasesPtr->evaluation));
    }
    
    return arc.num_phases;
}


/***************************************************************************
 * arc2mariadb_parseMagitude
 * Returns: -1 on error
 *           0 if ok
 * 
 ***************************************************************************/
int arc2mariadb_parseMagitude(char * msg, const char * tableName)
{
    MAG_INFO mag = {0};
    char query[MAX_STR];
    unsigned long qid;
    
    if (rd_mag(msg, strlen(msg), &mag))
        return -1;
    
    errno = 0; //Clear error flag
    qid = strtoul(mag.qid, NULL, 10); //Convert to number
    if (errno)
        return -1;
    
    
    snprintf(query, sizeof(query),
             "UPDATE %s SET magnitude = %f, magType = '%s' WHERE event_id = %lu",
             tableName, mag.mag, mag.szmagtype, qid);

    if (mysql_query(mariadb_conn, query) != 0) {
        return -1;
    }
    return 0;
}




int arc2mariadb_writeEvent(SQLeventEntry * event, const char * tableName)
{
    char query[MAX_STR * 8];
    
    // Build the INSERT statement
    snprintf(query, sizeof(query),
             "INSERT INTO %s (event_id, method, date, position, depth, magnitude, magType, phases, stdError, azimuthalGap, minimumDistance, e0, e0az, e0dip, e1, e1az, e1dip, e2, erh, erz) "
             "VALUES (%lu, '%s', '%s', ST_GeomFromText(CONCAT('POINT(', %f, ' ', %f, ')')), %f, %f, '%s', %d, %f, %d, %f, %f, %d, %d, %f, %d, %d, %f, %f, %f) "
             "ON DUPLICATE KEY UPDATE "
             "event_id = VALUES(event_id), method = VALUES(method), date = VALUES(date), position = VALUES(position), depth = VALUES(depth), magnitude = VALUES(magnitude), magType = VALUES(magType), phases = VALUES(phases), stdError = VALUES(stdError),  azimuthalGap = VALUES(azimuthalGap), minimumDistance = VALUES(minimumDistance), e0 = VALUES(e0), e0az = VALUES(e0az), e0dip = VALUES(e0dip), e1 = VALUES(e1), e1az = VALUES(e1az), e1dip = VALUES(e1dip), e2 = VALUES(e2), erh = VALUES(erh), erz = VALUES(erz)",
             tableName, event->event_id, event->method, event->date, event->lon, event->lat, event->depth, event->magnitude, event->magType, event->phases, event->stdError, event->azimuthalGap, event->minimumDistance, event->e0, event->e0az, event->e0dip, event->e1, event->e1az, event->e1dip, event->e2, event->erh, event->erz);

    // Execute the query
    if (mysql_query(mariadb_conn, query) != 0) {
        return -1;
    }
    return 0;
}



int arc2mariadb_writePhase(SQLphaseEntry * phase, const char * tableName)
{
    char query[MAX_STR * 8];
         
    // Build the INSERT statement
    snprintf(query, sizeof(query),
         "INSERT INTO %s (date, uncertainty, station, channel, network, location, onset, polarity, phase, event_id, azimuth, distance, takeoffangle, timeresidual, timeweight, evaluation) "
         "VALUES ('%s', %f, '%s', '%s', '%s', '%s', '%s', '%s', '%c', %lu, %u, %f, %u, %f, %f, '%s') "
         "ON DUPLICATE KEY UPDATE "
         "date = VALUES(date), "
         "uncertainty = VALUES(uncertainty), "
         "onset = VALUES(onset), "
         "polarity = VALUES(polarity), "
         "azimuth = VALUES(azimuth), "
         "distance = VALUES(distance), "
         "takeoffangle = VALUES(takeoffangle), "
         "timeresidual = VALUES(timeresidual), "
         "timeweight = VALUES(timeweight), "
         "evaluation = VALUES(evaluation)",
         tableName, phase->date, phase->uncertainty, phase->s, phase->c, phase->n, phase->l, phase->onset, phase->polarity, phase->phase, phase->event_id, phase->azimuth, phase->distance, phase->takeoffangle, phase->timeresidual, phase->timeweight, phase->evaluation);
    
    // Execute the query
    if (mysql_query(mariadb_conn, query) != 0) {
        return -1;
    }
    
    return 0;
}




/***************************************************************************
 * arc2mariadb_checkTable
 * 
 * Check table health. Return:
 * 0    Table OK
 * -1   Query failed, critical SQL engine error
 * 1    Table not OK. Need to create it
 ***************************************************************************/
int arc2mariadb_checkTable(const char * tableName)
{
    char query[MAX_STR];
    MYSQL_RES *result;
    MYSQL_ROW row;
    int retval;
    unsigned int columns;

    // Check if the table exists
    snprintf(query, sizeof(query), "CHECK TABLE %s", tableName);
    if (mysql_query(mariadb_conn, query) != 0) {
        return -1; // Query error
    }
    
    // Store the result of CHECK TABLE
    result = mysql_store_result(mariadb_conn);
    if (!result) {
        return -1; // Result error
    }

    // Get the result(s)
    columns = mysql_field_count(mariadb_conn);
    retval = 1;
    while ((row = mysql_fetch_row(result))) {
        if (strncmp(row[columns-1], "OK", strlen("OK")) == 0) {
            retval = 0; // Found OK string!
        }
    }

    // Free the result
    mysql_free_result(result);

    return retval;
}



int arc2mariadb_createTable(const char * tableName, const char * foreignTableName, const char * createStatement)
{
    char query[MAX_STR * 8];
    
    snprintf(query, sizeof(query), "CREATE TABLE %s %s", tableName, createStatement);
    
    // Execute the CREATE TABLE query
    if (mysql_query(mariadb_conn, query) != 0)
        return -1; // Error
    
    if (foreignTableName != NULL) { // FK defined, need to alter table
        snprintf(query, sizeof(query), "ALTER TABLE %s ADD CONSTRAINT %s_%s_FK FOREIGN KEY (event_id) REFERENCES %s (event_id) ON DELETE CASCADE ON UPDATE CASCADE", tableName, tableName, foreignTableName, foreignTableName);
        // Execute the ALTER TABLE query
        if (mysql_query(mariadb_conn, query) != 0)
            return -1; // Error
    }
    
    return 0;
}
