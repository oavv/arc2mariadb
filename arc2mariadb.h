/*
 *   $Id: arc2mariadb.h
 *
 *    Revision history:
 *
 *     Revision 1.0  2023/12/21 victor preatoni
 *     Initial revision
 *
 */

#ifndef __ARC2MARIADB_H
#define __ARC2MARIADB_H


/***************** DEFINES ********************/
#define ARC2MARIADB_VERSION "1.0.0 - 2023"
#define MAX_STR 255
#define MAX_LOGO   2
#define MARIADB_DEFAULT_PORT    3306

/* Error messages used by arc2mariadb
***********************************/
#define  ERR_MISSMSG       0   /* message missed in transport ring        */
#define  ERR_TOOBIG        1   /* retreived msg too large for buffer      */
#define  ERR_NOTRACK       2   /* msg retreived; tracking limit exceeded  */
#define  ERR_QUEUE         3   /* queue error                             */

/* Thread things
***************/
#define THREAD_STACK 8192
#define MSGSTK_OFF    0              /* MessageStacker has not been started      */
#define MSGSTK_ALIVE  1              /* MessageStacker alive and well            */
#define MSGSTK_ERR   -1              /* MessageStacker encountered error quit    */

thr_ret WriteThd(void *);            /* used to pass messages between main thread */
thr_ret MessageStacker(void *);      /*   and Dup thread */


/************* Typedefs ***********************/
typedef struct {
    unsigned long   event_id;
    char            method[32];
    char            date[24];
    float           lat;
    float           lon;
    float           depth;
    float           magnitude;
    char            magType[3];
    int             phases;
    float           stdError;
    int             azimuthalGap;
    float           minimumDistance;
    /* Error ellipse */
    float           e0;
    int             e0az;
    int             e0dip;
    float           e1;
    int             e1az;
    int             e1dip;
    float           e2;
    /* Position error */
    float           erh;
    float           erz;
} SQLeventEntry;

typedef struct {
    unsigned long   event_id;
    char            date[24];
    float           uncertainty;
    char            s[6];
    char            c[4];
    char            n[3];
    char            l[3];
    char            onset[20];
    char            polarity[20];
    char            phase;
    unsigned short  azimuth;
    float           distance;
    unsigned short  takeoffangle;
    float           timeresidual;
    float           timeweight;
    char            evaluation[20];
} SQLphaseEntry;

typedef struct {
    char * InstID;
    char * ModID;
    char * MsgType;
} MsgLogoStr;

typedef struct {
    char * InRing;            /* name of transport ring for input         */
    char * MyModName;         /* speak as this module name/id             */
    int    LogSwitch;         /* 0 if no logfile should be written        */
    int    DebugSwitch;       /* 1 to log some debug info                 */
    int    HeartBeatInt;      /* seconds between heartbeat messages       */
    unsigned long   MaxMsgSize;       /* max size for input/output msgs           */
    unsigned long    QueueSize;	      /* max messages in output circular buffer   */
    MsgLogoStr MsgLogo[MAX_LOGO];     /* Logos in string format           */
    unsigned short nLogo;     /* number of logos read                     */
    char * DBUsername;        /* MariaDB user with write access to DBName */
    char * DBHostname;        /* MariaDB hostname or IP address           */
    char * DBName;            /* MariaDB database name to write to        */
    char * DBPassword;        /* MariaDB DBUsername password              */
    char * DBeventsTable;     /* Name of table to store events            */
    char * DBphasesTable;     /* Name of table to store picks/phase arrivals */
    unsigned short DBPort;    /* MariaDB TCP listen port                  */
    char * MethodTag;         /* Tag to identify automatic locations in DB*/
    char * EvaluationTag;     /* Tag to identify automatic picks in DB    */
    char EvaluationChar;      /* Data Source char that will be converted  */
    float WeightTable[4];     /* Pick quality to time uncertainty conversion*/
} Config;

typedef struct {
    long InRingKey;             /* key of transport ring for input          */
    MSG_LOGO HeartLogo;         /* logo of heartbeat message                */
    MSG_LOGO ErrorLogo;         /* logo of error message                    */
    MSG_LOGO GetLogo[MAX_LOGO]; /* array for requesting module,type,instid  */
    unsigned short nLogo;       /* number of logos properly parsed          */
    unsigned char TypeError;
    unsigned char TypeHyp2000Arc;
    unsigned char TypeMagnitude;
} LookUp;


/* Functions in this source file
*******************************/
int arc2mariadb_config(char * configfile, Config *configPtr);
int arc2mariadb_lookup(Config *configPtr, LookUp *lookupPtr);
void arc2mariadb_status(MSG_LOGO *, char *);

int arc2mariadb_startDB(Config *configPtr);
void arc2mariadb_closeDB(void);
int arc2mariadb_parseHyp2000(char * msg, SQLeventEntry * event, SQLphaseEntry ** phasesPtr);
int arc2mariadb_parseMagitude(char * msg, const char * tableName);
int arc2mariadb_writeEvent(SQLeventEntry * event, const char * tableName);
int arc2mariadb_writePhase(SQLphaseEntry * phase, const char * tableName);
int arc2mariadb_checkTable(const char * tableName);
int arc2mariadb_createTable(const char * tableName, const char * foreignTableName, const char * createStatement);



#endif
