# This is arc2mariadb's parameter file

#  Basic Earthworm setup:
#
MyModuleId           MOD_ARC2MARIADB   # module id for this instance of arc2trig 
InRing               HYPO_RING   # shared memory ring for input
LogFile              1           # 0 to turn off disk log file; 1 to turn it on
                                 # 2 to log to module log but not to stderr/stdout
HeartBeatInt         30          # seconds between heartbeats

#Debug     1   # OPTION: Enable debugging info to be logged

# List the message logos to grab from transport ring. Only ARC and MAGNITUDE supported
#              Installation         Module          Message Types
GetMsgLogo    ${EW_INSTALLATION}    MOD_WILDCARD   TYPE_HYP2000ARC
GetMsgLogo    ${EW_INSTALLATION}    MOD_LOCALMAG   TYPE_MAGNITUDE


QueueSize         5000          # number of msgs to buffer. Useful in case of temporary
                                # database connection failures, arc2mariadb buffers the
                                # loading operations and postpone their executions until
                                # the connection was reestablished.


# MariaDB connection parameters
# Change according to your needs
DBHostname        "localhost"
DBName            myDBname
DBUsername        myDBuser
DBPassword        myDBpassword
#DBPort            3306

# Name of tables to store event and
# phase arrivals/picks associated to that event
# NOTE: if table does not exist, it will be created.
DBeventsTable     "events"
DBphasesTable     "phases"

# MethodTag: how do you want to tag HYP2000ARC locations in database.
MethodTag   "hyp2000"

# EvaluationTag: convert Data Source character from HYP2000_ARC message
#                to a string used to identify picks in database
#                This character is defined in eqassemble.d/eqproc.d line "DataSrc"
EvaluationTag   "W"     "ew_picker"

# WeightTable: assign a time uncertainty in seconds to pick qualities from 0 (best) to 3 (worst)
WeightTable 0.02 0.05 0.50 1.00
